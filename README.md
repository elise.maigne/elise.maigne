Je présente des trucs ici : [slides](https://forgemia.inra.fr/elisemaigne/slides) : dont [git](https://elisemaigne.pages.mia.inra.fr/slides/2021_git/#1), [renv](https://elisemaigne.pages.mia.inra.fr/slides/insee_package_renv/presentation.html) ou [sk8](https://elisemaigne.pages.mia.inra.fr/slides/presentation_sk8_miat/)

Je donne des formations comme ça : [data.table](https://forgemia.inra.fr/elisemaigne/learndatatable), [shiny](https://forgemia.inra.fr/elisemaigne/formation-shiny) ou [recherche reproductible](https://elise.maigne.pages.mia.inra.fr/rr_imabs/presentation/Presentation_atelier_RR_IMABS.html)

Je code souvent des trucs ici : [ASTERICS](https://forgemia.inra.fr/asterics), [HiCDOC](https://github.com/mzytnicki/HiCDOC), [SK8](https://forgemia.inra.fr/sk8), [MIAT](https://forgemia.inra.fr/miat-com), ...


L'application du l'excellent package R [ ̀{cols4all}`](https://mtennekes.github.io/cols4all/) pour trouver des palettes de couleurs inclusives : [https://elisemaigne.shinyapps.io/cols4all_app/](https://elisemaigne.shinyapps.io/cols4all_app/)